#!/usr/bin/env bash

set -o pipefail

JAR="/usr/local/share/openapi-generator-cli.jar"

java -ea ${JAVA_OPTS} -Xms512M -Xmx1024M -server -jar ${JAR} "$@"

FROM alpine:3.19

ARG GENERATOR_VERSION

COPY openapi-generator-cli.sh /usr/local/bin/openapi-generator-cli

RUN apk add --no-cache \
        make \
        bash \
        curl \
        jq \
        yq \
        openjdk11-jre-headless \
        nss \
    && curl -L -o /usr/local/share/openapi-generator-cli.jar https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/${GENERATOR_VERSION}/openapi-generator-cli-${GENERATOR_VERSION}.jar \
    && chmod +x /usr/local/bin/openapi-generator-cli

WORKDIR /src

ENTRYPOINT ["openapi-generator-cli"]

CMD ["help"]

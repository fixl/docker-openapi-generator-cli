# OpenAPI Generator Cli Image

[![pipeline status](https://gitlab.com/fixl/docker-openapi-generator-cli/badges/master/pipeline.svg)](https://gitlab.com/fixl/docker-openapi-generator-cli/-/pipelines)
[![version](https://fixl.gitlab.io/docker-openapi-generator-cli/version.svg)](https://gitlab.com/fixl/docker-openapi-generator-cli/-/commits/master)
[![size](https://fixl.gitlab.io/docker-openapi-generator-cli/size.svg)](https://gitlab.com/fixl/docker-openapi-generator-cli/-/commits/master)
[![Docker Pulls](https://img.shields.io/docker/pulls/fixl/openapi-generator-cli)](https://hub.docker.com/r/fixl/openapi-generator-cli)
[![Docker Stars](https://img.shields.io/docker/stars/fixl/openapi-generator-cli)](https://hub.docker.com/r/fixl/openapi-generator-cli)

A Docker container containing [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator) that can be using
along with [3 Musketeers](https://3musketeers.io/).


## Build the image

```bash
make build
```

## Generate code

```bash
docker run --rm -v ${PWD}:/src openapi-generator-cli generate \
    -i https://raw.githubusercontent.com/openapitools/openapi-generator/master/modules/openapi-generator/src/test/resources/2_0/petstore.yaml \
    -g go \
    -o /src/out/go
```
